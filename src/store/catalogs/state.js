export default function () {
  return {
    profiles: [],
    genders: [],
    countries: [],
    states: [],
    marital_statuses: [],
    units: [],
    cat_modules: [],
    months: [],
    cat_days: [],
    users: [],
    people: [],
    cat_doc_types: [],
    cat_recep_meds: [],
    cat_priorities: [],
    cat_attention_types: [],
    cat_instructions: [],
    cat_step_types: [],
    holidays: [],
    position: [],
    availableUsers: [],
    availablePosition: [],
    cat_external_entities: [],
    entities: [],
    years: ['2021', '2022', '2023', '2024', '2025', '2026', '2027', '2028', '2029', '2030'],
    external_people: [],
    external_companies: [],
    people_assistants: [],
    cat_rejection_reasons: [],
    unit_position: [],
    cat_external_doc_types: [],
    cat_external_priorities: [],
  }
}
