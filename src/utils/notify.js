import { Notify } from 'quasar'
import { i18n } from 'boot/i18n'

const notifyOptionsSuccess = {
  icon: 'fas fa-smile-beam',
  position: 'bottom-right',
  textColor: 'white',
  color: 'positive'
}

const notifyOptionsError = {
  icon: 'fas fa-frown',
  position: 'bottom-right',
  textColor: 'white',
  color: 'negative'
}

const notifyOptionsReminder = {
  icon: 'warning',
  position: 'bottom-right',
  textColor: 'white',
  color: 'secondary',
  timeout: 1000,
}

const notifyOptionsAlert = {
  icon: 'warning',
  position: 'bottom-right',
  textColor: 'black',
  color: 'warning',
}

export const notifySuccess = (message) => {
  Notify.create({
    ...notifyOptionsSuccess,
    message: message !== undefined ? message : i18n.messages[i18n.locale].success
  })
}

export const notifyError = (message) => {
  Notify.create({
    ...notifyOptionsError,
    message: message !== undefined ? message : i18n.messages[i18n.locale].error
  })
}

export const notifyReminder = (message) => {
  Notify.create({
    ...notifyOptionsReminder,
    message: message !== undefined ? message : i18n.messages.this[i18n.locale].reminder
  })
}

export const notifyAlert = (message) => {
  Notify.create({
    ...notifyOptionsAlert,
    message: message !== undefined ? message : i18n.messages[i18n.locale].alert
  })
}
