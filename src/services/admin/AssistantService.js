import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * get: /api/positions/{id} GET
  * */
const basePath = '/api/admin/assistants';

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`
  return axiosInstance.delete(URL).then((res) => res.data);
}
