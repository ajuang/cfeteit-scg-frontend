export default [
  { path: 'reception', component: () => import('components/reception/Index.vue') },
  { path: 'reception/create', component: () => import('components/reception/Create.vue') },
  { path: 'reception/:id/edit', component: () => import('components/reception/Edit.vue') },
  { path: 'reception/:id/reply', component: () => import('components/reception/Reply.vue') },
  { path: 'reception/:id/office', component: () => import('components/reception/Office.vue') },
];
