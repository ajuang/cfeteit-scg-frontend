import { axiosInstance } from 'boot/axios'

export const getStructure = () => {
  const URL = '/api/folio/get-structure';
  return axiosInstance.get(URL).then((res) => res.data);
};

export const consecutiveReserved = (payload) => {
  const URL = 'api/folio/consecutive-reserved';
  return axiosInstance.get(URL, payload).then((res) => res.data.consecutives);
};

export const bis = (payload) => {
  const URL = '/api/folio/bis';
  return axiosInstance.get(URL, payload).then((res) => res.data.consecutives);
};

export const getConsecutive = (payload) => {
  const URL = '/api/folio/get-consecutive';
  return axiosInstance.get(URL, payload).then((res) => res.data.consecutive);
};

export const getReserve = (payload) => {
  const URL = '/api/folio/reserve';
  return axiosInstance.get(URL, payload).then((res) => res.data.consecutives);
};

export const reserve = (payload) => {
  const URL = '/api/folio/reserve';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `/api/folio/reserve/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const getOutputFolios = () => {
  const URL = '/api/folio/get-output-affair-folios';
  return axiosInstance.get(URL).then((res) => res.data);
};
