import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * checksignKey: /api/checksignKey GET
  * checksignPost: /api/checksignPost POST
  * */

export const getChecksignKey = (payload) => {
  const URL = '/api/checksignKey';
  return axiosInstance.get(URL, payload).then((res) => res.data);
};

export const postChecksign = (payload) => {
  const URL = '/api/checksignPost';
  return axiosInstance.get(URL, payload).then((res) => res.data);
}
