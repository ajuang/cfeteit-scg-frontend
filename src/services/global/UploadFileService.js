import { axiosInstance } from 'boot/axios'

export const upload = (payload) => {
  const URL = '/api/files/upload';
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, payload, { headers }).then((res) => res.data.file);
};

export const upImage = (payload) => {
  const URL = '/api/upImage';
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, payload, { headers }).then((res) => res.data.file);
};

export const uploadContractExcel = (payload) => {
  const URL = '/api/contracts-file';
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, payload, { headers }).then((res) => res);
};

export const showFile = (id) => {
  const URL = `/api/files/affair/${id}`;
  return axiosInstance.get(URL, { responseType: 'blob' }).then((res) => res);
};

export const externalShowFile = (payload, id) => {
  const URL = `/api/externalOfficeFiles/externalOffice/${id}`;
  return axiosInstance.put(URL, payload, { responseType: 'blob' }).then((res) => res);
};

export const externalShowFileAnnexes = (id) => {
  const URL = `/api/externalOfficeFiles/externalOfficeAnnexes/${id}`;
  return axiosInstance.get(URL, { responseType: 'blob' }).then((res) => res);
};

export const externalSentFileShow = (id) => {
  const URL = `/api/externalSentFiles/showFile/${id}`;
  return axiosInstance.get(URL, { responseType: 'blob' }).then((res) => res);
};

export const externalSentFileAnnexesShow = (id) => {
  const URL = `/api/externalSentFiles/showFileAnnexes/${id}`;
  return axiosInstance.get(URL, { responseType: 'blob' }).then((res) => res);
};

export const destroy = (id) => {
  const URL = `/api/files/delete/${id}/annexed`
  return axiosInstance.delete(URL).then((res) => res.data);
};

export const customTemplate = (id) => {
  const URL = `/api/files/${id}/generate`
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, { headers }).then((res) => res.data.file);
}

export const fileAnnexes = (id) => {
  const URL = `/api/files/${id}/annexes`
  return axiosInstance.get(URL).then((res) => res.data);
}

export const showAnnexesByIdExternalOffice = (id) => {
  const URL = `/api/externalOfficeFiles/${id}/annexes`
  return axiosInstance.get(URL).then((res) => res.data);
}

export const showAnnexesByIdExternalAffairSent = (id) => {
  const URL = `/api/externalSentFiles/${id}/annexes`
  return axiosInstance.get(URL).then((res) => res.data);
}
