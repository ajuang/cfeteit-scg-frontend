describe('edit folioStructure', () => {
  it('login', () => {
    // login
    cy.visit('/login')
    cy.get('#username').type('usuario1.generico')
    cy.get('#password').type('12345678')
    cy.get('#login').click()
  })

  it('edit folioStructure', () => {
    // form
    cy.get('#config').click()
    cy.get('.admin-list > :nth-child(4)').click()
    cy.get('.row.q-col-gutter-sm').contains('Estructura de folios').click()
    cy.get('.bg-grey-3 > .q-col-gutter-sm > :nth-child(2) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get(':nth-child(7) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get(':nth-child(3) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get(':nth-child(7) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get(':nth-child(4) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get(':nth-child(7) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get('.q-col-gutter-sm > :nth-child(5) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get('.q-btn').contains('Guardar').click()
    cy.get('.row.q-col-gutter-sm').contains('Estructura de folios')
  })

  it('logout', () => {
    // logout
    cy.get('#profile').click()
    cy.get('#logout').click()
    cy.get('.q-card__actions').contains('Cerrar Sesión').click()
  })
})