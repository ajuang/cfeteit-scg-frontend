import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * update: /api/affair-participation PUT
  * */
const basePath = '/api/affair-participation';

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
