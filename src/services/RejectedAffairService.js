import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/rejectedAffair GET
  * get: /api/rejectedAffair/{id} GET
  * store: /api/rejectedAffair POST
  * update: /api/rejectedAffair PUT
  * destroy: /api/rejectedAffair DELETE
  * */

const basePath = '/api/admin/affairRejects';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.affairReject);
}

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const edit = (endpoint) => {
  console.log(endpoint);
  return axiosInstance.get(endpoint).then((res) => res.data.affairReject);
}

export const destroy = (id) => {
  const URL = `/api/admin/rejectedAffair/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data);
}

export const rejectedBox = (payload) => {
  const URL = '/api/admin/rejectedBox';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairReject);
}
