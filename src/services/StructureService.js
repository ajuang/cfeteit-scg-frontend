import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/admin/structures GET
  * get: /api/admin/structures/{id} GET
  * store: /api/admin/structures POST
  * update: /api/admin/structures PUT
  * destroy: /api/admin/structures DELETE
  * */
const basePath = '/api/admin/structures';

export const index = (payload) => {
  const URL = `${basePath}`;
  return axiosInstance.get(URL, payload).then((res) => res.data.users);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const updateNode = (payload) => {
  const URL = `${basePath}/update/node`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const destroy = (id) => {
  const URL = `/api/admin/structures/${id}`
  return axiosInstance.delete(URL).then((res) => res.data);
};

export const getLabelsToAttendant = (payload) => {
  const URL = `${basePath}/getLabelsToAttendant`;
  return axiosInstance.post(URL, payload).then((res) => res.data);
}
