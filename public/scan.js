/* eslint-disable */
window.URL = window.URL || window.webkitURL;
var wsImpl = window.WebSocket || window.MozWebSocket;
window.ws = new wsImpl('ws://localhost:8181/');
var toPDF = []
ws.onmessage = function(e){
  if (e.data instanceof Blob){
    let reader = new FileReader();
    let mydiv = document.createElement('div')
    let scroll_img = document.getElementById('scroll-img')
    let list = scroll_img.childNodes[0].childNodes
    let img_pdf = document.createElement('img')
    reader.readAsDataURL(e.data)
    reader.onloadend = () => {
      toPDF.push(reader.result)
    }
    img_pdf.src = window.URL.createObjectURL(e.data)
    img_pdf.width = 600
    img_pdf.height = 800
    img_pdf.style.border = 'thick solid #621132';
    mydiv.className = 'text-center'
    mydiv.appendChild(img_pdf)
    list[0].appendChild(mydiv)
  }
};
ws.onopen = function(){
  console.log('open')
};
ws.onerror = function(e){
  console.log(e)
}

closeCollab = () => {
  const collab = document.getElementById('collabora-online-viewer');
  if (collab) {
    window.location.replace(window.location.origin)
  }
}