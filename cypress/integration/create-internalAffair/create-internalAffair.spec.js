describe('internal Affair', () => {
  it('login', () => {
    // login
    cy.visit('/login')
    cy.get('#username').type('usuario1.generico')
    cy.get('#password').type('12345678')
    cy.get('#login').click()
  })

  it('internalForm', () => {
    // form
    const num = '2'
    cy.get('.q-ml-sm > .q-btn__wrapper > .q-btn__content').click()
    cy.get('.text-grey-8 > :nth-child(1)').click()
    cy.get('form')
    cy.get(':nth-child(1) > [data-v-689a31f6=""] > :nth-child(1) > :nth-child(1) > .q-card > :nth-child(1) > .q-btn > .q-btn__wrapper > .q-btn__content > .q-icon').click()
    cy.get('#addressee').click()
    cy.contains('Jefa de Disciplina').click()
    cy.get('.q-btn').contains('Agregar').click()
    cy.get('#docTypeIn').click()
    cy.contains('Volante').click()
    cy.get('#prioIn').click()
    cy.contains('Alta').click()
    cy.contains('Folio/Control Interno').type(`FolioInterno${num}`)
    cy.contains('Palabras clave').type('Palabra0{enter}')
    cy.contains('Palabras clave').type('Palabra1{enter}')
    cy.contains('Asunto').type(`Asunto${num}`)
    cy.get('section > .col-xs-12 > .q-field > .q-field__inner').type(`Comentario${num}`)
    cy.get('.col-sm-1 > .q-btn > .q-btn__wrapper > .q-btn__content > .q-icon').click()
    cy.get('.q-btn').contains('Siguiente').click()
  })

  // it('logout', () => {
  //   // logout
  //   cy.get('#profile').click()
  //   cy.get('#logout').click()
  //   cy.get('.q-card__actions').contains('Cerrar Sesión').click()
  // })
})