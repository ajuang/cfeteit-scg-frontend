import { axiosInstance } from 'boot/axios'

export const upload = (payload) => {
  const URL = '/api/attached-files/upload';
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, payload, { headers }).then((res) => res.data.file);
};

export const destroy = (id) => {
  const URL = `/api/attached-files/${id}`
  return axiosInstance.delete(URL).then((res) => res.data);
};

export const fileAnnexes = (id) => {
  const URL = `/api/attached-files/${id}`
  return axiosInstance.get(URL).then((res) => res.data);
}

export const destroyInputAffairFile = (payload) => {
  const URL = '/api/attached-files/delete-input-affairs'
  return axiosInstance.post(URL, payload).then((res) => res.data);
}
export const getCompresedFile = (id) => {
  const URL = `/api/attached-files/getCompresedFile/${id}`;
  return axiosInstance.get(URL, { responseType: 'blob' }).then((res) => res.data);
}
