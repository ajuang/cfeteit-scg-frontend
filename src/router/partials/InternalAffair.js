export default [
  { path: 'internal-affair/create', component: () => import('components/internalAffair/Create.vue') },
  { path: 'internal-affair/:id/edit', component: () => import('components/internalAffair/Edit.vue') },
  { path: 'internal-affair/:id/office', component: () => import('components/internalAffair/Office.vue') },
  { path: 'internal-affair/:id/reply', component: () => import('components/internalAffair/Reply.vue') },
  { path: 'internal-affair/:id/repair', component: () => import('components/internalAffair/Repair.vue') }
];
