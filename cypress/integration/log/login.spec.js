describe('login', () => {
  it('Obtener por medio de un id', () => {
    cy.visit('/login')
    cy.get('#username').type('david.pantoja')
    cy.get('#password').type('12345678')
    cy.get('#login').click()
    cy.get('#profile').click()
    cy.get('#logout').click()
    cy.get('.q-card__actions').contains('Cerrar Sesión').click()
  })
})