export default [
  { path: '/archive', component: () => import('components/Archive/Index.vue') },
  // Section
  { path: '/archive/section', component: () => import('src/components/Archive/Section/Index.vue') },
  { path: '/archive/section/create', component: () => import('src/components/Archive/Section/Create.vue') },
  { path: '/archive/section/:id/edit', component: () => import('src/components/Archive/Section/Edit.vue') },
  // Subserie
  { path: '/archive/subseries', component: () => import('components/Archive/Subserie/Index.vue') },
  { path: '/archive/subseries/create', component: () => import('components/Archive/Subserie/Create.vue') },
  { path: '/archive/subseries/:id/edit', component: () => import('components/Archive/Subserie/Edit.vue') },
  // CoverCatalog
  { path: '/archive/CoverCatalog', component: () => import('components/Archive/CoverCatalog/Index.vue') },
  { path: '/archive/CoverCatalog/create', component: () => import('components/Archive/CoverCatalog/Create.vue') },
  // GeneratingArea
  { path: '/archive/GeneratingArea', component: () => import('components/Archive/GeneratingArea/Index.vue') },
  { path: '/archive/GeneratingArea/create', component: () => import('components/Archive/GeneratingArea/Create.vue') },
  { path: '/archive/GeneratingArea/:id/edit', component: () => import('components/Archive/GeneratingArea/Edit.vue') },
  // InformationCharacter
  { path: '/archive/InformationCharacter', component: () => import('components/Archive/InformationCharacter/Index.vue') },
  { path: '/archive/InformationCharacter/create', component: () => import('components/Archive/InformationCharacter/Create.vue') },
  { path: '/archive/InformationCharacter/:id/edit', component: () => import('components/Archive/InformationCharacter/Edit.vue') },
  // FolderQuery
  { path: '/archive/FolderQuery', component: () => import('components/Archive/FolderQuery/Index.vue') },
  { path: '/archive/FolderQuery/files', component: () => import('components/Archive/FolderQuery/ViewFiles.vue') },
];
