export default function () {
  return {
    users: [],
    information: {
      name: '',
      firstName: '',
      secondName: '',
      position: '',
      position_id: '',
      username: '',
      semblance: '',
      mobile_phone: '',
      has_two_fa: false,
      permissions: [],
      assistants: [],
    },
    qrCode: null,
    usr: null,
  }
}
