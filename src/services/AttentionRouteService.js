import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/attentionRoutes GET
  * get: /api/attentionRoutes/{id} GET
  * store: /api/attentionRoutes POST
  * update: /api/attentionRoutes PUT
  * destroy: /api/attentionRoutes DELETE
  * */
export const index = (payload) => {
  const URL = '/api/attentionRoutes';
  return axiosInstance.get(URL, payload).then((res) => res.data.routes);
}

export const store = (payload) => {
  const URL = '/api/attentionRoutes';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const edit = (id) => {
  const URL = `/api/attentionRoutes/${id}/edit`
  return axiosInstance.get(URL).then((res) => res.data.route);
}

export const update = (payload, id) => {
  const URL = `/api/attentionRoutes/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const destroy = (id) => {
  const URL = `/api/attentionRoutes/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data);
}
