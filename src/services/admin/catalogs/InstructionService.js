import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/instructions GET
  * get: /api/catalogs/instructions/{id} GET
  * store: /api/catalogs/instructions POST
  * update: /api/catalogs/instructions PUT
  * destroy: /api/catalogs/instructions DELETE
  * */
const basePath = '/api/admin/catalogs/instructions';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.instructions);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.instruction);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.instructions)
}

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const getInstructions = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.instructions);
};
