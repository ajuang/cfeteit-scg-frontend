# develop stage
FROM node:14.18.0-alpine as develop-stage
WORKDIR /app
COPY package*.json ./
COPY quasar.conf*.js ./
RUN yarn global add @quasar/cli
COPY . .
RUN npm install && npm install --only=dev
# build stage
FROM develop-stage as build-stage
RUN yarn
RUN yarn add --dev dotenv
RUN quasar build

# production stage
FROM registry.access.redhat.com/ubi7/nginx-120@sha256:aac5d95d3a1e01a1d1352516a1d00fb0e8a6c407a004d0594147723bdc0ad9cb as production-stage
user 0
COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf
USER 0
RUN rm -rf /usr/share/nginx/html/*
USER 0
COPY --from=build-stage /app/dist/spa /usr/share/nginx/html
USER 0
RUN ls /usr/share/nginx/html
USER 0

EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
