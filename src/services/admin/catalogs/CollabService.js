import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/collabora-direction GET
  * get: /api/catalogs/collabora-direction/{id} GET
  * store: /api/catalogs/collabora-direction POST
  * update: /api/catalogs/collabora-direction PUT
  * destroy: /api/catalogs/collabora-direction DELETE
  * */
const basePath = '/api/admin/catalogs/collabora-direction';

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.collabdirection);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
