import { axiosInstance } from 'boot/axios'

import * as UserService from 'src/services/admin/UserService';

export function getUsers(context, payload) {
  UserService.index(payload).then((users) => {
    context.commit('setUsers', users)
  }).catch((err) => {
    alert(err)
  })
}

export function getCode(context, payload) {
  return UserService.getCode(payload).then((res) => {
    console.error('Log an error level message.');
    return res.code
  }).catch((err) => {
    alert(err)
  })
}

export function login(context, credentials) {
  const endPoint = '/api/login';
  return axiosInstance.post(endPoint, credentials)
    .then((res) => {
      const isAutenticated = res.data.authenticated;
      if (isAutenticated) {
        const userData = res.data.user;
        context.commit('setUserInformation', userData)
        window.sessionStorage.setItem('grp_token', res.data.grp_token);
        window.sessionStorage.setItem('grp_token_expiration', res.data.grp_token_expiration);
        window.sessionStorage.setItem('position_id', res.data.user.position_id);
        window.sessionStorage.setItem('position_name', res.data.user.position);
        window.sessionStorage.setItem('grp_token_expiration', res.data.grp_token_expiration);
        window.sessionStorage.setItem('from_user_id', res.data.user.from_user_id);
        window.sessionStorage.setItem('full_name_label', '');
        window.sessionStorage.setItem('email_label', '');
        window.localStorage.setItem('username', res.data.user.username);
        axiosInstance.defaults.headers.common.Authorization = `Bearer ${res.data.grp_token}`;
      }
      const { setTwoFA } = res.data;
      if (setTwoFA) {
        context.commit('setQRCode', res.data.qrCode);
        context.commit('setUsr', res.data.usr);
        this.$router.push('2fa')
      }
      return res.data;
    }).catch((err) => err.response.data.message);
}
export function forgot(context, payload) {
  const endPoint = '/api/sendPasswordResetLink';
  return axiosInstance.post(endPoint, payload)
    .then((res) => {
      const isVerify = res.data.success;
      if (isVerify) {
        console.log(isVerify)
      }
      return res.data;
    }).catch((err) => err.response.data.message);
}
export function reset(context, payload) {
  const endPoint = '/api/resetPassword';
  return axiosInstance.post(endPoint, payload)
    .then((res) => {
      const isVerify = res.data.success;
      if (isVerify) {
        console.log(isVerify)
      }
      return res.data;
    }).catch((err) => err.response.data.message);
}

export function verify(context, credentials) {
  const endPoint = '/api/verify'
  return axiosInstance.post(endPoint, credentials)
    .then((res) => {
      const isAuthenticated = res.data.authenticated;
      if (isAuthenticated) {
        const userData = res.data.user;
        context.commit('setUserInformation', userData)
        window.sessionStorage.setItem('grp_token', res.data.grp_token);
        window.sessionStorage.setItem('grp_token_expiration', res.data.grp_token_expiration);
        axiosInstance.defaults.headers.common.Authorization = `Bearer ${res.data.grp_token}`;
      }
      return res.data;
    });
}

export function refreshInformation(context) {
  const type = sessionStorage.getItem('type')
  const endPoint = `/api/getUserInfo/0/${type}`;
  axiosInstance.get(endPoint)
    .then((res) => {
      const userData = res.data.user;
      context.commit('setUserInformation', userData)
    })
    .catch(() => {
      this.loadingState = false;
    });
}

export function clearInformation(context, id) {
  const endPoint = '/api/logout';
  axiosInstance.post(endPoint, id)
    .then(() => {
      context.commit('setUserInformation', {});
      sessionStorage.removeItem('grp_token');
      sessionStorage.clear();
    })
    .catch(() => {
      this.loadingState = false;
    });
}
