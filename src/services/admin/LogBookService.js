import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/user GET
  * get: /api/user/{id} GET
  * store: /api/user POST
  * update: /api/user PUT
  * destroy: /api/user DELETE
  * */

export const index = (payload) => {
  const URL = '/api/admin/logbook';
  return axiosInstance.get(URL, payload).then((res) => res.data.log);
};

export const store = (payload) => {
  const URL = '/api/admin/logbook';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};
