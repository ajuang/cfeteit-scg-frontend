import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * get: /api/input-digital-affair/{id} GET
  * */
const basePath = '/api/input-digital-affair';

export const edit = (payload, id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL, payload).then((res) => res.data.affair);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const getReplydata = (payload, id) => {
  const URL = `${basePath}/${id}/reply-data`;
  return axiosInstance.get(URL, payload).then((res) => res.data.affair);
};

export const globalsearch = (payload) => {
  const URL = '/api/input-digital-affair/globalSearch';
  return axiosInstance.get(URL, payload).then((res) => res.data.input_digital_affairs);
};

export const getHistory = (id) => {
  const URL = `${basePath}/${id}/getHistory`;
  return axiosInstance.get(URL).then((res) => res.data);
};
