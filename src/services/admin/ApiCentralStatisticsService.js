import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/statistics GET
  * */
export const getApiCentralStatistics = (payload) => {
  const URL = '/api/statistics/api-central';
  return axiosInstance.get(URL, payload).then((res) => res.data);
}

export const getDataDocuments = (payload) => {
  const URL = '/api/statistics/api-central/documents';
  return axiosInstance.get(URL, payload).then((res) => res.data.data);
}

export const retryAffair = (payload, id) => {
  const URL = `/api/statistics/api-central/retry-affair/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
}
