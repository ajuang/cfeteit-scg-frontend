const initialState = {
  id: '',
  name: '',
  subject: '',
  description: '',
  turn_from_id: '',
  folio: '',
  key_words: [],
  cat_priorities_id: '',
  recep_med_id: '',
  reception_date: '',
  reception_time: '',
  attention_date: '',
  document_date: '',
  cat_doc_types_id: '',
  doc_to_id: '',
  affair_type: '',
  sender_id: '',
  receiver_id: '',
  ccp_id: '',
  externals: '',
  archive: false,
  fund_id: '',
  serie_id: '',
  comment: '',
  external_people_id: '',
  external_company_id: '',
  folio_control: '',
  folio_associated: '',
}

export const updateReception = (state, value) => {
  state = Object.assign(state, value);
}

// eslint-disable-next-line no-unused-vars
export const reset = (state) => {
  state = Object.assign(state, initialState);
}

/* Se combina el estado actual de los externalPeople con los nuevos que se generan  */
export function setExternalPeople(state, externalPeople) {
  state = Object.assign(state, externalPeople)
}

/* Se combina el estado actual de los externalCompany con los nuevos que se generan  */
export function setExternalCompanies(state, externalCompany) {
  state = Object.assign(state, externalCompany)
}
