describe('cedit dependence', () => {
  it('login', () => {
    // login
    cy.visit('/login')
    cy.get('#username').type('usuario1.generico')
    cy.get('#password').type('12345678')
    cy.get('#login').click()
  })

  it('edit dependence', () => {
    // form
    const num = '1'
    cy.get('#config').click()
    cy.get('.admin-list > :nth-child(4)').click()
    cy.get('.row.q-col-gutter-sm').contains('Dependencia').click()
    cy.get('form').contains('Nombre').type(`Dependencia${num}`)
    cy.contains('Acrónimo').type(`DEP${num}`)
    cy.get('.q-btn').contains('Guardar').click()
    cy.get('.row.q-col-gutter-sm').contains('Dependencia')
  })

  it('logout', () => {
    // logout
    cy.get('#profile').click()
    cy.get('#logout').click()
    cy.get('.q-card__actions').contains('Cerrar Sesión').click()
  })
})