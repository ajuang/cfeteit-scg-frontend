export default [
  { path: 'admin/cat/entities', component: () => import('components/admin/cats/entities/Index.vue') },
  { path: 'admin/cat/entities/edit', component: () => import('components/admin/cats/entities/Edit.vue') }
];
