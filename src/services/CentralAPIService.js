import { axiosInstance } from 'boot/axios'

const basePath = '/api/admin/centralapi';

export const get = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data);
};

export const getEntities = (payload) => {
  const URL = '/api/admin/centralEntities';
  return axiosInstance.get(URL, payload).then((res) => res.data);
};

export const sendExternalAffair = (payload) => {
  const URL = '/api/admin/sendExternalAffair';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};
