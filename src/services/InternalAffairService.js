import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/internal-affair GET
  * get: /api/internal-affair/{id} GET
  * store: /api/internal-affair POST
  * update: /api/internal-affair PUT
  * destroy: /api/internal-affair DELETE
  * */
const basePath = '/api/internal-affair';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};

export const edit = (payload, id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL, payload).then((res) => res.data.affair);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.date);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const updateSenders = (payload, id) => {
  const URL = `${basePath}/${id}/sender-signature`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const scheduleDocument = () => {
  const URL = '/api/admin/affairReceptionsScheduleDocument';
  return axiosInstance.get(URL).then((res) => res.data);
};

export const scheduleShipment = (payload, id) => {
  const URL = `${basePath}/ScheduleShipment/${id}`;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const getReplydata = (payload, id) => {
  const URL = `${basePath}/${id}/reply-data`;
  return axiosInstance.get(URL, payload).then((res) => res.data.affair);
};

export const getHistory = (id) => {
  const URL = `${basePath}/${id}/history`;
  return axiosInstance.get(URL).then((res) => res.data);
};

// export const toHistorical = (payload, id) => {
//   const URL = `${basePath}/${id}/to-historical`;
//   return axiosInstance.put(URL, payload).then((res) => res.data);
// };

export const guestFinishCollab = (payload, id) => {
  const URL = `${basePath}/${id}/guest-finish-collab`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const collabHistory = (payload, id) => {
  const URL = `${basePath}/${id}/collaboration-history`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const repair = (payload, id) => {
  const URL = `${basePath}/${id}/repair`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
