import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/catalogs GET
  * get: /api/catalogs/{id} GET
  * store: /api/catalogs POST
  * update: /api/catalogs PUT
  * destroy: /api/catalogs DELETE
  * */

export const index = (payload) => {
  const URL = '/api/admin/catalogs';
  return axiosInstance.get(URL, payload).then((res) => res.data.catalogs);
};

export const getStates = (payload) => {
  const URL = `/api/admin/catalogs/get-states/${payload}`;
  return axiosInstance.get(URL).then((res) => res.data.states);
}

export const getResponsible = (payload, attendant = true) => {
  const URL = `/api/admin/catalogs/get-responsible/${payload}/${attendant}`;
  return axiosInstance.get(URL).then((res) => res.data.people);
}

export const byUnit = (id) => {
  const URL = `/api/admin/catalogs/get-responsible/unit/${id}`;
  return axiosInstance.get(URL).then((res) => res.data.people);
};

export const getPositions = (payload) => {
  const URL = `/api/admin/catalogs/get-positions/${payload}`;
  return axiosInstance.get(URL).then((res) => res.data.positions);
}

export const getUnits = (payload) => {
  const URL = `/api/admin/catalogs/get-units/${payload}`;
  return axiosInstance.get(URL).then((res) => res.data.units);
}

export const getPositionsforturn = () => {
  const URL = '/api/admin/catalogs/get-positions-for-turn';
  return axiosInstance.get(URL).then((res) => res.data.position);
}

export const getUserInfo = (id, type) => {
  const URL = `/api/getUserInfo/${id}/${type}`;
  return axiosInstance.get(URL).then((res) => res.data.user);
}

export const getExternalUnits = (payload) => {
  const URL = `/api/admin/catalogs/get-external-units/${payload}`;
  return axiosInstance.get(URL).then((res) => res.data.units);
}
