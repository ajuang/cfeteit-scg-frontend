import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/catalogs GET
  * get: /api/catalogs/{id} GET
  * store: /api/catalogs POST
  * update: /api/catalogs PUT
  * destroy: /api/catalogs DELETE
  * */

const basePath = 'api/admin/reservedConsecutives';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.consecutives);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.affair);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.date);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `/api/admin/folioConsecutives/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const restore = (payload) => {
  const URL = 'api/admin/restoreConsecutive';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const updateStatus = (payload) => {
  const URL = 'api/admin/updateReservedConsecutiveStatus';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const consecutiveToSelect = (payload) => {
  const URL = 'api/admin/consecutiveToSelect';
  return axiosInstance.get(URL, payload).then((res) => res.data.consecutives);
};
