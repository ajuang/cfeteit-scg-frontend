import { axiosInstance } from 'boot/axios'

export const upload = (payload) => {
  const URL = '/api/avatar/upload';
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, payload, { headers }).then((res) => res.data.upload);
};

export const getAvatar = (payload) => {
  const URL = '/api/avatar/get';
  return axiosInstance.get(URL, payload).then((res) => res.data.avatar);
};
