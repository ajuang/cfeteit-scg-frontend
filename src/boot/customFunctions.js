import Vue from 'vue';
import moment from 'moment';

function cleanStr(cad) {
  const nCad = cad.replace(/[^-\sa-zA-Z0-9 /;,."#$%=¿?!¡+*:_&()' {} \u005B \u005D \u00C0-\u024F]/g, '')
    .replace(/\s+/g, ' ')
  return nCad
}

function calcDaysAdd(fecha, nDias) {
  let resultado = ''
  const fechaString = fecha.toString();
  const newDate = moment(fechaString, 'YYYY/MM/DD').add(nDias, 'days');
  const day = newDate.format('DD');
  const month = newDate.format('MM');
  const year = newDate.format('YYYY');
  const joined = [year, month, day].join('/');
  resultado = `${joined}`;
  console.log(nDias);
  console.log(resultado);
  return resultado;
}

function existHoliday(date, holiday) {
  let number = 0;
  for (let i = 1; i <= 6; i += 1) {
    const newdate = calcDaysAdd(date, i);
    for (let k = 0; k <= (holiday.length - 1); k += 1) {
      const value = holiday[k].holiday_date.replaceAll('-', '/')
      if (value === newdate) {
        number += 1;
      }
    }
  }
  return number;
}
function finalDate(date, dias) {
  const newdate = calcDaysAdd(date, dias)
  return newdate;
}
function enableDate(permissionArr, holidaysArr, date) {
  const arr = holidaysArr.map((obj) => obj.holiday_date.replace(/-/g, '/'))
  if (!permissionArr.includes('send-Holiday')) {
    date = (arr.includes(date)) ? false : date
  }
  if (!permissionArr.includes('send-Weekend')) {
    const dateObj = new Date(date)
    date = (dateObj.getDay() === 0 || dateObj.getDay() === 6) ? false : date
  }
  return date
}

Vue.prototype.$cleanStr = cleanStr;
export { cleanStr, enableDate, finalDate, existHoliday }
