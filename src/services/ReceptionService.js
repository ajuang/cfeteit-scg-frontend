import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/affairReceptions GET
  * get: /api/affairReceptions/{id} GET
  * store: /api/affairReceptions POST
  * update: /api/affairReceptions PUT
  * destroy: /api/affairReceptions DELETE
  * */
export const index = (payload) => {
  const URL = '/api/admin/affairReceptions';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
}

export const store = (payload) => {
  const URL = '/api/admin/affairReceptions';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const edit = (endpoint) => {
  console.log(endpoint);
  return axiosInstance.get(endpoint).then((res) => res.data.affair);
}

export const destroy = (id) => {
  const URL = `/api/admin/affairReceptions/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data);
}
