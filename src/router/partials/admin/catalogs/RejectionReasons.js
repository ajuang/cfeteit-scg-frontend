export default [
  { path: 'admin/cat/rejectionReasons', component: () => import('components/admin/cats/rejectionReasons/Index.vue') },
  { path: 'admin/cat/rejectionReasons/create', component: () => import('components/admin/cats/rejectionReasons/Create.vue') },
  { path: 'admin/cat/rejectionReasons/:id/edit', component: () => import('components/admin/cats/rejectionReasons/Edit.vue') },
];
