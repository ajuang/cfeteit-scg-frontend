export default [
  { path: 'attention-route', component: () => import('components/attentionRoute/Index.vue') },
  { path: 'attention-route/create', component: () => import('components/attentionRoute/Create.vue') },
  { path: 'attention-route/create-to-send', component: () => import('components/attentionRoute/CreateToSend.vue') },
  { path: 'attention-route/:id/edit', component: () => import('components/attentionRoute/Edit.vue') },
];
