import { axiosInstance } from 'boot/axios'

export const upload = (payload) => {
  const URL = '/api/externalFiles/upload';
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, payload, { headers }).then((res) => res.data.file);
};

export const uploadContractExcel = (payload) => {
  const URL = '/api/contracts-file';
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, payload, { headers }).then((res) => res);
};

export const template = () => {
  const URL = '/api/externalFiles/template/';
  return axiosInstance.get(URL, { responseType: 'blob' }).then((res) => res);
};

export const customTemplate = (id) => {
  const URL = `/api/externalFiles/${id}/generate`
  const headers = {
    'Content-Type': 'multipart/form-data'
  };
  return axiosInstance.post(URL, { headers }).then((res) => res.data.file);
}

export const fileExist = (id) => {
  const URL = `/api/externalFiles/${id}/exist`
  return axiosInstance.get(URL).then((res) => res.data);
}
