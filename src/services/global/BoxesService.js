import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * assigned: /api/assigned GET
  * */
export const assigned = (payload) => {
  const URL = '/api/assigned';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
}

export const turn = (payload) => {
  const URL = '/api/admin/affairReceptionsTurn';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairsInbox);
};

export const internalOutbox = (payload) => {
  const URL = '/api/internalOutbox';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
}

export const externalOutbox = (payload) => {
  const URL = '/api/externalOutbox';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
}

export const externalDigitalInbox = (payload) => {
  const URL = '/api/externalDigitalInbox';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
}

export const collabora = (payload) => {
  const URL = '/api/collabora';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
}

export const historical = (payload) => {
  const URL = '/api/historical';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
}
