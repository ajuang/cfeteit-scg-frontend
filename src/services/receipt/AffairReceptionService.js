import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/document GET
  * get: /api/catalogs/document/{id} GET
  * store: /api/catalogs/document POST
  * update: /api/catalogs/document PUT
  * destroy: /api/catalogs/document DELETE
  * */
const basePath = '/api/admin/affairReceptions';

export const get = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data);
};

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};

export const getAffairReceptionByIdUser = (payload) => {
  const URL = '/api/admin/getAffairReceptionByIdUser';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.affair);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.date);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `/api/admin/affairReceptions/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const inbox = (payload) => {
  const URL = '/api/admin/affairReceptionsInbox';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairsInbox);
};

export const inboxByIdUser = (payload) => {
  const URL = '/api/admin/affairReceptionsInboxByIdUser';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairsInbox);
};

export const turn = (payload) => {
  const URL = '/api/admin/affairReceptionsTurn';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairsInbox);
};

export const inboxHistory = (payload) => {
  const URL = '/api/admin/affairReceptionsInboxHistory';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairsInbox);
};

export const priorityBox = (payload) => {
  const URL = '/api/admin/affairReceptionsPriorities';
  return axiosInstance.get(URL, payload).then((res) => res.data.priorityAffairs);
};

export const unitAffairs = (payload) => {
  const URL = '/api/admin/unit-affairs';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};

export const showHistory = (folio) => {
  const URL = `${basePath}/showHistory/${folio}`;
  return axiosInstance.get(URL).then((res) => res.data.affairs);
};

export const getAffairReceptionByFolio = (folio) => {
  const URL = `${basePath}/getAffairReceptionByFolio/${folio}`;
  return axiosInstance.get(URL).then((res) => res.data.affairs);
};

export const generateFolio = () => {
  const URL = '/api/admin/affairReceptionsGenerateFolio';
  return axiosInstance.get(URL).then((res) => res.data);
};

export const scheduleDocument = () => {
  const URL = '/api/admin/affairReceptionsScheduleDocument';
  return axiosInstance.get(URL).then((res) => res.data);
};

export const scheduleShipment = (payload, id) => {
  const URL = `${basePath}/ScheduleShipment/${id}`;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const getCollaborators = (id) => {
  const URL = `${basePath}/getCollaborators/${id}`;
  return axiosInstance.get(URL).then((res) => res.data.collaborators);
};

export const indexToBis = (payload) => {
  const URL = '/api/admin/affairsToBis';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};
