import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/holiday GET
  * get: /api/catalogs/holiday/{id} GET
  * store: /api/catalogs/holiday POST
  * update: /api/catalogs/holiday PUT
  * destroy: /api/catalogs/holiday DELETE
  * */
const basePath = '/api/admin/catalogs/holidays';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.holidays);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.holiday);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.holidays)
}

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const getHolidays = () => {
  const URL = '/api/admin/catalogs/getHolidays';
  return axiosInstance.get(URL).then((res) => res.data.holidays);
};
