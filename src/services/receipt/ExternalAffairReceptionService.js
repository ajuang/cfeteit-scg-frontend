import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/document GET
  * get: /api/catalogs/document/{id} GET
  * store: /api/catalogs/document POST
  * update: /api/catalogs/document PUT
  * destroy: /api/catalogs/document DELETE
  * */
const basePath = '/api/admin/externalAffairReceptions';

export const get = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data);
};

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.affair);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.date);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const saveDraft = (payload) => {
  const URL = '/api/admin/externalAffairReceptions/saveDraft';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const updateForm = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const inbox = (payload) => {
  const URL = '/api/admin/externalAffairReceptionsInbox';
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};

export const saveInputAffair = (id) => {
  const URL = `/api/admin/externalAffairReceptions/saveToSend/${id}`
  console.log(URL);
  return axiosInstance.get(URL).then((res) => res.data);
};
export const OriginalFile = (id) => {
  const URL = `/api/original-document/${id}`
  console.log(URL);
  return axiosInstance.get(URL).then((res) => res.data);
};
export const OriginalFileSigned = (id) => {
  const URL = `/api/document-signed/${id}`
  console.log(URL);
  return axiosInstance.get(URL).then((res) => res.data);
};

export const getReplydata = (payload, id) => {
  const URL = `${basePath}/${id}/reply-data`;
  return axiosInstance.get(URL, payload).then((res) => res.data.affair);
};

export const getHistory = (id) => {
  const URL = `${basePath}/${id}/getHistory`;
  return axiosInstance.get(URL).then((res) => res.data);
};
