export default [
  { path: 'profile', component: () => import('components/profile/Index.vue') },
  { path: 'profile/create', component: () => import('components/profile/Create.vue') },
  { path: 'profile/:id/edit', component: () => import('components/profile/Edit.vue') },
];
