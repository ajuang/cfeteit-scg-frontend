export default [
  { path: 'answer-affair/:id', component: () => import('components/answerAffair/Answer.vue') },
  { path: 'answer-affair/create', component: () => import('components/answerAffair/Create.vue') },
  { path: 'answer-affair/:id/edit', component: () => import('components/answerAffair/Edit.vue') }
];
