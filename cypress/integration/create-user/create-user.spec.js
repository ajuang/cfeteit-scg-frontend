describe('Crear y editar nuevo usuario', () => {
  const num = '107'
  let field = ['Usuario', 'Contraseña', 'Nombre(s)', 'Apellido Paterno', 'Apellido Materno', 'Correo electrónico', 'Teléfono', 'CURP', 'RFC']
  let val = [`Usuario${num}.generico`, '12345678', `Nombre${num}`, `Paterno${num}`, `Materno${num}`, `usuario${num}@email.com`, '(55)-11223344', 'mesy941029mdfcgs08', 'mesy941029cb6']
  let newVal = [`NewUsuario${num}.generico`, '87654321', `Nombre${num}new`, `Paterno${num}new`, `Materno${num}new`, `newusuario${num}@email.com`, '(55) 4433 - 2211', 'mesy941029mdfcgs09', 'mesy941029cb7']
  it('Iniciar sesión', () => {
    cy.visit('/login')
    cy.get('#username').type('usuario1.generico')
    cy.get('#password').type('12345678')
    cy.get('#login').click()
  })

  it('Crear usuario', () => {
    cy.get('#config').click()
    cy.get('.admin-list > :nth-child(3)').click()
    cy.get('.row.q-col-gutter-sm').contains('Creación de Usuarios').click()
    cy.get('.float-right').click()
    field.forEach((element, index) => {
      cy.get('form').contains(element).type(val[index])
    })
    cy.get('.q-select__dropdown-icon').click()
    cy.contains('Administrador').click()
    cy.get('.q-btn').contains('Guardar').click()
  })

  it('Verificar registro creado', () => {
    cy.get('input[placeholder*="Buscar"]').type(val[5])
    cy.get('.q-table').contains(`Nombre${num}`)
    cy.get('.q-table').find('tr').should('have.length', 2)
    cy.get('.q-btn.bg-primary').click()
    field.forEach((element, index) => {
      if (element != 'Contraseña') {
        cy.get(`input[aria-label*="${element}"]`).should(($value) => {
          expect($value.val()).to.include(val[index])
        })
      }
    })
    cy.get('.q-field__native > span').should('have.text', 'Administrador')
    cy.get('.q-btn').contains('Cancelar').click()
  })

  it('Editar registro', () => {
    let email = `usuario${num}@email.com`
    field.forEach((element, index) => {
      cy.get('input[placeholder*="Buscar"]').type(email)
      cy.get('.q-table').contains(`Nombre${num}`)
      cy.get('.q-table').find('tr').should('have.length', 2)
      cy.get('.q-btn.bg-primary').click()
      if (element != 'Contraseña') {
        cy.get(`input[aria-label*="${element}"]`).should(($value) => {
          expect($value.val()).to.include(val[index])
        })
      }
      cy.get(`input[aria-label*="${element}"]`).clear()
      cy.get('form').contains(element).type(newVal[index])
      cy.get('.q-btn').contains('Guardar').click()
      if (element === 'Correo electrónico') {
        email = newVal[5]
      }
      if (element != 'Contraseña') {
        cy.get('input[placeholder*="Buscar"]').type(email)
        cy.get('.q-table').contains(`Nombre${num}`)
        cy.get('.q-table').find('tr').should('have.length', 2)
        cy.get('.q-btn.bg-primary').click()
        cy.get(`input[aria-label*="${element}"]`).should(($value) => {
          expect($value.val()).to.include(newVal[index])
        })
        cy.get('.q-btn').contains('Cancelar').click()
      }
    })
    cy.get('input[placeholder*="Buscar"]').type(email)
    cy.get('.q-table').contains(`Nombre${num}`)
    cy.get('.q-table').find('tr').should('have.length', 2)
    cy.get('.q-btn.bg-primary').click()
    cy.get('.q-field__native > span').should('have.text', 'Administrador')
    cy.get('.q-select__dropdown-icon').click()
    cy.get('.q-menu').contains('Usuario').click()
    cy.get('.q-btn').contains('Guardar').click()
    cy.get('input[placeholder*="Buscar"]').type(email)
    cy.get('.q-table').contains(`Nombre${num}`)
    cy.get('.q-table').find('tr').should('have.length', 2)
    cy.get('.q-btn.bg-primary').click()
    cy.get('.q-field__native > span').should('have.text', 'Usuario')
    cy.get('.q-btn').contains('Cancelar').click()
  })


  it('Cerrar sesión', () => {
    cy.visit('/')
    cy.get('#profile').click()
    cy.get('#logout').click()
    cy.get('.q-card__actions').contains('Cerrar Sesión').click()
  })
})
