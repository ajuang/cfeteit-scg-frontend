export default [
  { path: 'admin/cat/folioStructure', component: () => import('components/admin/cats/folios/Index.vue') },
  { path: 'admin/cat/folioStructure/:id/edit', component: () => import('components/admin/cats/folios/Edit.vue') },
  { path: 'foliosReserve', component: () => import('components/admin/cats/folios/reserve/Table.vue') }
];
