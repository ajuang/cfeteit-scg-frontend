export default [

  // ExternalMoral
  { path: 'admin/cat/external/moral', component: () => import('components/admin/cats/external/moral/Index.vue') },
  { path: 'admin/cat/external/moral/:id/edit', component: () => import('components/admin/cats/external/moral/Edit.vue') },

  // ExternalPeople
  { path: 'admin/cat/external/people', component: () => import('components/admin/cats/external/people/Index.vue') },
  { path: 'admin/cat/external/people/:id/edit', component: () => import('components/admin/cats/external/people/Edit.vue') },

  // ExternalDependency
  { path: 'admin/cat/external/dependency', component: () => import('components/admin/cats/external/dependency/Index.vue') },
  { path: 'admin/cat/external/dependency/:id/edit', component: () => import('components/admin/cats/external/dependency/Edit.vue') },

  // ExternalUnits
  { path: 'admin/cat/external/units', component: () => import('components/admin/cats/external/units/Index.vue') },
  { path: 'admin/cat/external/units/:id/edit', component: () => import('components/admin/cats/external/units/Edit.vue') },
];
