export default [
  { path: 'admin/cat/priorities', component: () => import('components/admin/cats/priorities/Index.vue') },
  { path: 'admin/cat/priorities/create', component: () => import('components/admin/cats/priorities/Create.vue') },
  { path: 'admin/cat/priorities/:id/edit', component: () => import('components/admin/cats/priorities/Edit.vue') },

];
