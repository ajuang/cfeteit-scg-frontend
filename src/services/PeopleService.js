import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/user GET
  * get: /api/user/{id} GET
  * store: /api/user POST
  * update: /api/user PUT
  * destroy: /api/user DELETE
  * */

export const index = (payload) => {
  const URL = '/api/admin/people';
  return axiosInstance.get(URL, payload).then((res) => res.data.people);
};

export const store = (payload) => {
  const URL = '/api/admin/people';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const save = (payload) => {
  const URL = '/api/peopleStore';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const edit = (id) => {
  const URL = `/api/admin/people/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.people);
};

export const update = (payload, id) => {
  const URL = `/api/admin/people/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const destroy = (id) => {
  const URL = `/api/admin/people/${id}`
  return axiosInstance.delete(URL).then((res) => res.data);
};

export const getPeopleAuth = () => {
  const URL = '/api/admin/getPeopleAuth';
  return axiosInstance.get(URL).then((res) => res.data);
}

export const getAssistantByResponsable = (responsable) => {
  const URL = `/api/admin/getAssistantByResponsable/${responsable}`
  return axiosInstance.get(URL).then((res) => res.data);
}

export const getPermissions = (id, model) => {
  const URL = `/api/admin/people/permissions/${id}/${model}`
  return axiosInstance.get(URL).then((res) => res.data.permissions);
}

export const updatePermissions = (payload, id) => {
  const URL = `/api/admin/people/permissions/${id}/update`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
