export default [
  { path: 'adminInterface/catalogs', component: () => import('components/AdminInterface/AdminInterfaceCatalogs/Index.vue') },
  { path: 'adminInterface/catalogs/collabora-direction', component: () => import('components/AdminInterface/AdminInterfaceCatalogs/Collabora.vue') }
];
