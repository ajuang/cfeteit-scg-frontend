export default [
  { path: 'admin/assistants', component: () => import('components/admin/assistants/Index.vue') },
  { path: 'admin/assistants/:id/edit', component: () => import('components/admin/assistants/Edit.vue') },
  { path: 'admin/assistants/:id/permission', component: () => import('components/admin/assistants/Permissions.vue') },
];
