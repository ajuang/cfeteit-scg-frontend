import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/doc-types GET
  * get: /api/catalogs/doc-types/{id} GET
  * store: /api/catalogs/doc-types POST
  * update: /api/catalogs/doc-types PUT
  * destroy: /api/catalogs/doc-types DELETE
  * */
const basePath = '/api/admin/catalogs/templates';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.docTypes);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.template);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.templates)
}

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
