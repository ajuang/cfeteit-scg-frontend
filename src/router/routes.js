import Admin from './partials/Admin';
import Positions from './partials/Positions';
import Calendar from './partials/Calendar';
import Reception from './partials/Reception';
import AttentionRoute from './partials/AttentionRoute';
import Structure from './partials/Structure';
import Boxes from './partials/Boxes';
import Assistants from './partials/Assistants';
import Collabora from './partials/Collabora';
import AnswerAffairs from './partials/AnswerAffairs';
import InternalAffair from './partials/InternalAffair';
import AdminInterface from './partials/AdminInterface';
import AdminInterfaceCatalogs from './partials/AdminInterfaceCatalogs';
import AdminInterfaceUsers from './partials/AdminInterfaceUsers';
import Profile from './partials/Profile';
import CentralAPI from './partials/CentralAPI';
import InputDigitalAffair from './partials/InputDigitalAffair';
import ExternalOffice from './partials/ExternalOffice';
import HelpPage from './partials/HelpPage';
import Statistics from './partials/Statistics';
import LogBooks from './partials/LogBooks';
import OutpuAffair from './partials/OutputAffair';
import Archive from './partials/ArchiveCatalog';

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      ...Admin,
      ...Positions,
      ...Calendar,
      ...Reception,
      ...AttentionRoute,
      ...Structure,
      ...Boxes,
      ...Assistants,
      ...AnswerAffairs,
      ...InternalAffair,
      ...AdminInterface,
      ...AdminInterfaceCatalogs,
      ...AdminInterfaceUsers,
      ...Collabora,
      ...Profile,
      ...CentralAPI,
      ...InputDigitalAffair,
      ...ExternalOffice,
      ...HelpPage,
      ...Statistics,
      ...LogBooks,
      ...OutpuAffair,
      ...Archive,
    ],
    beforeEnter: (to, from, next) => {
      if (sessionStorage.getItem('grp_token') && sessionStorage.getItem('position_id')) {
        next();
      } else {
        next('/login');
      }
    },
  },
  {
    path: '/login',
    component: () => import('pages/Login.vue'),
    beforeEnter: (to, from, next) => {
      if (sessionStorage.getItem('grp_token') && sessionStorage.getItem('position_id')) {
        next('/');
      } else {
        next();
      }
    },
    children: [
      { path: '', component: () => import('pages/Index.vue') },
    ],
  }, {
    path: '/forgot',
    component: () => import('pages/Forgot.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
    ],
  }, {
    path: '/recovery',
    component: () => import('pages/Recovery.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
    ],
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue'),
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes;
