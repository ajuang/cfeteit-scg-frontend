export default [
  { path: 'admin/cat/templates', component: () => import('components/admin/cats/templates/Index.vue') },
  { path: 'admin/cat/templates/:id/edit', component: () => import('components/admin/cats/templates/Edit.vue') },
  { path: 'admin/cat/templates/create', component: () => import('components/admin/cats/templates/Create.vue') },
];
