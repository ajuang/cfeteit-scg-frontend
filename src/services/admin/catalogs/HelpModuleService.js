import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/modules GET
  * get: /api/modules/{id} GET
  * store: /api/modules POST
  * update: /api/modules PUT
  * destroy: /api/modules DELETE
  * */
const basePath = '/api/admin/helpModules';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data);
};

export const getHelpElements = (id) => {
  const URL = `${basePath}/${id}/HelpElements`;
  return axiosInstance.get(URL).then((res) => res.data.helpElements);
};

export const getHelpElementSections = (id) => {
  const URL = `${basePath}/${id}/HelpElementSections`;
  return axiosInstance.get(URL).then((res) => res.data.helpElementSections);
};

export const getHelpSectionImage = (id) => {
  const URL = `${basePath}/${id}/HelpSectionImage`;
  return axiosInstance.get(URL).then((res) => res.data.helpSectionImage);
};
