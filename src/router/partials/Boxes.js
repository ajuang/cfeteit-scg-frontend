export default [
  { path: 'outbox', component: () => import('components/boxes/outbox/Index.vue') },
  { path: 'inbox', component: () => import('src/components/boxes/inbox/Index.vue') },
  { path: 'historical', component: () => import('src/components/boxes/historical/Index.vue') },
];
