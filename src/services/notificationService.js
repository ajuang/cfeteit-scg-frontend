import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/turn GET
  * get: /api/turn/{id} GET
  * store: /api/turn POST
  * update: /api/turn PUT
  * destroy: /api/turn DELETE
  * */
export const getNotification = (id) => {
  const URL = `/api/notifications/${id}`;
  return axiosInstance.get(URL).then((res) => res.data);
};
