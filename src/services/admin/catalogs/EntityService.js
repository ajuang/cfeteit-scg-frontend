import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/entities GET
  * get: /api/catalogs/entities/{id} GET
  * store: /api/catalogs/entities POST
  * update: /api/catalogs/entities PUT
  * destroy: /api/catalogs/entities DELETE
  * */
const basePath = '/api/admin/catalogs/entities';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.entities);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.entity);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.entities)
}

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const getEntitybyAuth = () => {
  const URL = '/api/admin/getEntitybyAuth';
  return axiosInstance.get(URL).then((res) => res.data.entity);
};

export const storeExternalDependency = (payload) => {
  const URL = '/api/admin/catalogs/external-entities';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};
