import Documents from './admin/catalogs/Documents';
import Templates from './admin/catalogs/Templates';
import Profiles from './admin/catalogs/Profiles';
import Test from './admin/catalogs/Test';
import SubjectTypes from './admin/catalogs/SubjectTypes';
import Entity from './admin/catalogs/Entities';
import Instruction from './admin/catalogs/Instructions';
import Holidays from './admin/catalogs/Holidays';
import RejectionReasons from './admin/catalogs/RejectionReasons';
import Units from './admin/catalogs/Units';
import Priorities from './admin/catalogs/Priorities';
import Folios from './admin/catalogs/Folios';
import ExternalReceptor from './admin/catalogs/ExternalReceptor';

export default [
  { path: 'admin/users', component: () => import('components/admin/users/Index.vue') },
  { path: 'admin/users/create', component: () => import('components/admin/users/Create.vue') },
  { path: 'admin/users/:id/edit', component: () => import('components/admin/users/Edit.vue') },
  { path: 'admin/users/:id/permission', component: () => import('components/admin/users/Permission.vue') },
  { path: 'admin/settings', component: () => import('components/admin/settings/Index.vue') },
  ...Documents,
  ...Templates,
  ...Profiles,
  ...Test,
  ...SubjectTypes,
  ...Entity,
  ...Instruction,
  ...Holidays,
  ...RejectionReasons,
  ...Units,
  ...Priorities,
  ...Folios,
  ...ExternalReceptor
];
