export default [
  { path: 'admin/cat/instructions', component: () => import('components/admin/cats/instructions/Index.vue') },
  { path: 'admin/cat/instructions/create', component: () => import('components/admin/cats/instructions/Create.vue') },
  { path: 'admin/cat/instructions/:id/edit', component: () => import('components/admin/cats/instructions/Edit.vue') }
];
