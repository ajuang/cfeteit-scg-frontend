import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * update: /api/affair-digital-participats PUT
  * */
const basePath = '/api/affair-digital-participats';

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
