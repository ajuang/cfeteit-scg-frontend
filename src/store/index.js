import Vue from 'vue';
import Vuex from 'vuex';

import catalogs from './catalogs';
import user from './user';
import config from './config';
import 'leaflet/dist/leaflet.css';
import reception from './reception';

Vue.use(Vuex);

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */
// initial state
const initialState = {
  catalogs: catalogs.state,
  user: user.state,
  config: config.state,
  reception: reception.state
}

const Store = new Vuex.Store({
  modules: {
    catalogs,
    user,
    config,
    reception,
  },
  mutations: {
    reset(state) {
      Object.keys(state).forEach((key) => {
        Object.assign(state[key], initialState[key])
      })
    }
  },
  // enable strict mode (adds overhead!)
  // for dev mode only
  strict: false,
});

export default function () {
  return Store
}

export { Store }
