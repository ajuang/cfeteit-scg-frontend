import { axiosInstance } from 'boot/axios'

import * as ReceptionService from 'src/services/ReceptionService';
import * as ExternalCompanyService from 'src/services/ExternalCompanyService';
import * as ExternalPeopleService from 'src/services/ExternalPeopleService';

export function save(context, reception) {
  const endPoint = '/api/admin/affairReceptions';
  return axiosInstance.post(endPoint, reception).then((res) => res).catch((error) => error.response);
}

export function edit(context, payload) {
  const endPoint = `/api/admin/affairReceptions/${payload.params}/edit`;
  ReceptionService.edit(endPoint).then((reception) => {
    context.commit('updateReception', reception);
  }).catch((err) => {
    alert(err);
  });
}

export function update(context, reception) {
  const endPoint = `/api/admin/affairReceptions/${reception.id}`;
  return axiosInstance.put(endPoint, reception)
    .then((res) => res).catch((err) => {
      console.log(err)
      return false;
    });
}

export function setExternalPeople(context, payload) {
  ExternalPeopleService.index(payload).then((externalPeople) => {
    context.commit('setExternalPeople', externalPeople)
  }).catch((err) => {
    alert(err)
  })
}

export function setExternalCompanies(context, payload) {
  ExternalCompanyService.index(payload).then((externalCompany) => {
    context.commit('setExternalCompanies', externalCompany)
  }).catch((err) => {
    alert(err)
  })
}
