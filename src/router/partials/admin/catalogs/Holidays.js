export default [
  { path: 'admin/cat/holidays', component: () => import('components/admin/cats/holidays/Index.vue') },
  { path: 'admin/cat/holidays/create', component: () => import('components/admin/cats/holidays/Create.vue') },
  { path: 'admin/cat/holidays/:id/edit', component: () => import('components/admin/cats/holidays/Edit.vue') },
];
