export default [
  { path: 'helpPage', component: () => import('components/helpPage/Index.vue') },

  // // others
  // { path: 'helpPage/contact', component: () => import('components/helpPage/others/contact.vue') },
  // { path: 'helpPage/addFiles', component: () => import('components/helpPage/others/addFiles.vue') },
  { path: 'helpPage/:id/helpElement', component: () => import('components/helpPage/HelpElement.vue') },
];
