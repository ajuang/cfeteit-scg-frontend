describe('create new instruction', () => {
  it('login', () => {
    // login
    cy.visit('/login')
    cy.get('#username').type('usuario1.generico')
    cy.get('#password').type('12345678')
    cy.get('#login').click()
  })

  it('create instruction', () => {
    // form
    const num = '1'
    cy.get('#config').click()
    cy.get('.admin-list > :nth-child(4)').click()
    cy.get('.row.q-col-gutter-sm').contains('Instrucciones').click()
    cy.get('.float-right').click()
    cy.get('form').contains('Instrucción').type(`Instrucción${num}`)
    cy.get('.q-select__dropdown-icon').click()
    cy.contains('UNIDAD DE ADMINISTRACIÓN Y FINANZAS').click()
    cy.get('.q-btn').contains('Guardar').click()
    cy.get('.q-table').contains(`Instrucción${num}`)
  })

  it('logout', () => {
    // logout
    cy.get('#profile').click()
    cy.get('#logout').click()
    cy.get('.q-card__actions').contains('Cerrar Sesión').click()
  })
})