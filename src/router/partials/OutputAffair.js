export default [
  { path: 'output-affair/create', component: () => import('components/outputAffair/Create.vue') },
  { path: 'output-affair/:id/edit', component: () => import('components/outputAffair/Edit.vue') },
  { path: 'output-affair/:id/office', component: () => import('components/outputAffair/Office.vue') },
  { path: 'output-affair/:id/reply', component: () => import('components/outputAffair/Reply.vue') },
];
