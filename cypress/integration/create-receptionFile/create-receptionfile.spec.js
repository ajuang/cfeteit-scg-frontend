describe('receptionFile', () => {
  it('login', () => {
    // login
    cy.visit('/login')
    cy.get('#username').type('usuario1.generico')
    cy.get('#password').type('12345678')
    cy.get('#login').click()
  })

  it('receptionForm', () => {
    // form
    const num = '4'
    cy.get('.q-ml-sm > .q-btn__wrapper > .q-btn__content').click()
    cy.get('.text-grey-8 > :nth-child(3)').click()
    cy.get('form')
    cy.get(':nth-child(1) > .q-card > :nth-child(1) > .q-btn > .q-btn__wrapper > .q-btn__content').click()
    cy.get('#addressee').click()
    cy.contains('Jefa de Disciplina').click()
    cy.get('.q-btn').contains('Agregar').click()
    cy.get('#RemType').click()
    cy.contains('Persona Moral').click()
    cy.get('.q-pa-md > .q-btn > .q-btn__wrapper > .q-btn__content > .q-icon').click()
    cy.contains('Nombre').type(`Nombre${num}`)
    cy.contains('Razón Social').type(`Razón Social${num}`)
    cy.contains('RFC').type(`RFC${num}`)
    cy.contains('Dirección').type(`Dirección${num}`)
    cy.contains('Email').type(`Email${num}@email.com`)
    cy.contains('Teléfono').type('5511223344')
    cy.get('.q-btn').contains('Guardar').click()
    cy.get('#moralPerson').click()
    cy.contains(`Razón Social${num}`).click()
    cy.contains('Folio del Documento').type(`Folio${num}`)
    cy.get('#MedRecep').click()
    cy.contains('Fisico/Escrito').click()
    cy.get('#prioRecep').click()
    cy.contains('Alta').click()
    cy.get('#docTypeRecep').click()
    cy.contains('Oficio').click()
    cy.get(':nth-child(7) > .q-field > .q-field__inner > .q-field__control > .q-field__append > .cursor-pointer').click()
    cy.get('.q-time__clock-pos-12 > span').dblclick('center', { force: true })
    cy.get('.q-btn').contains('Cerrar').click()
    cy.contains('Asunto').type(`Prueba${num}`)
    cy.contains('Descripción').type(`Descripción${num}`)
    cy.contains('Folio/Control Interno').type(`FolioCon${num}`)
    //addPDFs
    // cy.get(':nth-child(2) > [data-v-4923c328=""] > .q-card__section > .q-col-gutter-sm > :nth-child(1) > .q-field > .q-field__inner > .q-field__control').click()
    // cy.get(':nth-child(2) > [data-v-4923c328=""] > .q-card__section > .q-col-gutter-sm > :nth-child(2) > .q-field > .q-field__inner > .q-field__control').click()
    // cy.get(':nth-child(2) > [data-v-4923c328=""] > .q-card__section > .q-col-gutter-sm > :nth-child(3) > .q-field > .q-field__inner > .q-field__control').click()
    //finPDFs
    cy.get('section > .col-xs-12 > .q-field > .q-field__inner > .q-field__control').type(`Comentario${num}`)
    cy.get('.col-sm-1 > .q-btn > .q-btn__wrapper > .q-btn__content > .q-icon').click()
    cy.get('.q-btn').contains('Enviar').click()
    cy.get('tbody > :nth-child(1) > :nth-child(1)').dblclick('center', { force: true })
    cy.get('#Conclude').click()

    
  })

  // it('logout', () => {
  //   // logout
  //   cy.get('#profile').click()
  //   cy.get('#logout').click()
  //   cy.get('.q-card__actions').contains('Cerrar Sesión').click()
  // })
})