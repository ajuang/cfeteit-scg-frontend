import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/output-affair GET
  * get: /api/output-affair/{id} GET
  * store: /api/output-affair POST
  * update: /api/output-affair PUT
  * destroy: /api/output-affair DELETE
  * */
const basePath = '/api/output-affair';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.affairs);
};

export const edit = (payload, id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL, payload).then((res) => res.data.affair);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.date);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const getHistory = (id) => {
  const URL = `${basePath}/${id}/getHistory`;
  return axiosInstance.get(URL).then((res) => res.data);
};
