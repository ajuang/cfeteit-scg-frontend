import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/turn GET
  * get: /api/turn/{id} GET
  * store: /api/turn POST
  * update: /api/turn PUT
  * destroy: /api/turn DELETE
  * */
export const index = (payload) => {
  const URL = '/api/turn';
  return axiosInstance.get(URL, payload).then((res) => res.data.turn);
}

export const store = (payload) => {
  const URL = '/api/turn';
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const edit = (payload, id) => {
  const URL = `/api/turn/${id}/edit`
  return axiosInstance.get(URL, payload).then((res) => res.data.turns);
}

export const update = (payload, id) => {
  const URL = `/api/turn/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const destroy = (id) => {
  const URL = `/api/turn/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data);
}

export const getLog = (id) => {
  const URL = `/api/turn/${id}/getLog`;
  return axiosInstance.get(URL).then((res) => res.data);
}
