import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * index: /api/catalogs/rejection-reasons GET
  * get: /api/catalogs/rejection-reasons/{id} GET
  * store: /api/catalogs/rejection-reasons POST
  * update: /api/catalogs/rejection-reasons PUT
  * destroy: /api/catalogs/rejection-reasons DELETE
  * */
const basePath = '/api/admin/catalogs/rejection-reasons';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.reasons);
};

export const edit = (id) => {
  const URL = `${basePath}/${id}/edit`;
  return axiosInstance.get(URL).then((res) => res.data.reason);
};

export const destroy = (id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.delete(URL).then((res) => res.data.reasons)
}

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
