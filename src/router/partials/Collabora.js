export default [
  { path: 'collabora/:id/edit', component: () => import('src/components/collabora/Collabora.vue') },
  { path: 'collabora-output-affair/:id/edit', component: () => import('src/components/collabora/collaboraOutputAffair.vue') },
];
