export default [
  { path: 'admin/cat/subjectTypes', component: () => import('components/admin/cats/subjectTypes/Index.vue') },
  { path: 'admin/cat/subjectTypes/create', component: () => import('components/admin/cats/subjectTypes/Create.vue') },
  { path: 'admin/cat/subjectTypes/:id/edit', component: () => import('components/admin/cats/subjectTypes/Edit.vue') },
];
