import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente

  * index: /api/statistics GET
  * */
export const getStatistics = (payload) => {
  const URL = '/api/statistics';
  return axiosInstance.get(URL, payload).then((res) => res.data);
}
export const getUnitStatistics = (payload) => {
  const URL = '/api/statistics/unit';
  return axiosInstance.get(URL, payload).then((res) => res.data);
}
export const getMemberStatistics = (id) => {
  const URL = `/api/statistics/member/${id}`;
  return axiosInstance.get(URL).then((res) => res.data);
}
export const getMemberExport = (id) => {
  const URL = `/api/statistics/getExcelMember/${id}`;
  return axiosInstance.get(URL, { responseType: 'blob' }).then((res) => res.data);
}
export const getScheduleByWeek = (payload, week) => {
  const URL = `/api/statistics/getScheduleByWeek/${week}`;
  return axiosInstance.get(URL, payload).then((res) => res.data);
}
export const getUnitScheduleByWeek = (payload, week) => {
  const URL = `/api/statistics/getUnitScheduleByWeek/${week}`;
  return axiosInstance.get(URL, payload).then((res) => res.data);
}
