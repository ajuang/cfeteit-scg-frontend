import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * update: /api/affair PUT
  * */
const basePath = '/api/affair';

export const finishCollabora = (payload, id) => {
  const URL = `${basePath}/${id}/finish-collabora`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};
