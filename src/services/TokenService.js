import { axiosInstance } from 'boot/axios'

/* La nomenclatura sugerida es la siguiente
  * getToken: /api/access-token/get-token/{id} GET
  * */
const basePath = '/api/access-token';

export const index = (payload) => {
  const URL = basePath;
  return axiosInstance.get(URL, payload).then((res) => res.data.permissions);
};

export const store = (payload) => {
  const URL = basePath;
  return axiosInstance.post(URL, payload).then((res) => res.data);
};

export const getToken = (id) => {
  const URL = `${basePath}/get-token/${id}`;
  return axiosInstance.get(URL).then((res) => res.data);
};

export const getTokenSender = (payload, id) => {
  const URL = `${basePath}/get-token-sender/${id}/`;
  return axiosInstance.get(URL, payload).then((res) => res.data);
};

export const getCollaborators = (id) => {
  const URL = `${basePath}/get-collaborators/${id}`;
  return axiosInstance.get(URL).then((res) => res.data);
};

export const getData = (id) => {
  const URL = `${basePath}/get-data/${id}`;
  return axiosInstance.get(URL).then((res) => res.data);
};

export const update = (payload, id) => {
  const URL = `${basePath}/${id}`;
  return axiosInstance.put(URL, payload).then((res) => res.data.permission);
};

export const updateStatus = (id, status) => {
  const URL = `${basePath}/update/status/${id}/${status}`;
  return axiosInstance.put(URL).then((res) => res.data);
};

export const destroy = (id) => {
  const URL = `/api/access-token/${id}`
  return axiosInstance.delete(URL).then((res) => res.data);
}

export const backSender = (payload, id) => {
  const URL = `${basePath}/${id}/back-sender`;
  return axiosInstance.put(URL, payload).then((res) => res.data);
};

export const getTokenOutputAffair = (id) => {
  const URL = `${basePath}/get-token-output-affair/${id}`;
  return axiosInstance.get(URL).then((res) => res.data);
}
